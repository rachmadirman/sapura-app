import React from 'react';
import { useNavigate } from "react-router-dom";
import FooterSmall from "./FooterSmall";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";

const HomePageExist = () => {

  const navigate = useNavigate();

  const navigateToRegister = () => {
    navigate("/RegisterConfig")
  }

  return (
    <>
      <section className="absolute w-full h-full bg-izeno-gradien-1">

        <div className='ml-64'>
          <Navbar />
        </div>

        <div className='flex-1 p-8 flex justify-center items-center ml-40'>
          <div class="container max-w-4xl bg-izeno-white relative shadow-lg rounded-lg border-0">
            <h1 class="text-5xl font-bold pt-4 mx-auto">Welcome to e-Invoice</h1>

            <h2 class="text-2xl font-md mb-2 pt-4 text-izeno-black">Getting Started</h2>
            <p class="text-izeno-black mb-2">This platform is created by iZeno to help you in submitting or accepting e-invoice to LHDN.</p>
            <p class="text-izeno-black mb-2">By using this platform as users you can upload your invoice automatically and manually through our system
              . This application only accept file in format : csv, json, and xml. Please, <a className="text-blue-700" href='https://sdk.myinvois.hasil.gov.my/documents/invoice-v1-0/'> click here </a>
              to get more details on document format.
            </p>
            <hr className="mt-3 border-b-1 border-gray-600" />
            <h2 class="text-2xl font-md mb-2 pt-2 text-izeno-black">User Guide</h2>
            <p class="text-izeno-black mb-2">For new user please don't forget to set up your configuration by clicking register button below.</p>
            <button onClick={navigateToRegister} class="mb-4 mt-2 text-white bg-izeno-red hover:bg-red-900 focus:ring-2 focus:outline-none focus:ring-red-900 font-medium rounded-lg text-sm px-3 py-2.5 text-center">
              Register</button>
          </div>
        </div>

        <Sidebar />
        <FooterSmall absolute />
      </section>
    </>
  );
}

export default HomePageExist;
