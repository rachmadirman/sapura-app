import React, { useState } from 'react';
import { Navigate, useNavigate } from "react-router-dom";
import FooterSmall from "./FooterSmall";
import Navbar from "./Navbar";
import axios from "axios";


const HomePage = () => {

  const [account, setAccount] = useState('')
  const [database, setDatabase] = useState('')
  const [schema, setSchema] = useState('')

  const [hoveredField, setHoveredField] = useState(null);
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false)

  const role = localStorage.getItem("storedValue")
  console.log("current role : " + role)

  const handleMouseEnter = (field) => {
    setHoveredField(field);

  };

  const handleMouseLeave = () => {
    setHoveredField(null);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // Call fetch all API
      setLoading(true)
      console.log("url = http://backend-api-route.apps.cluster-bp2lg.bp2lg.sandbox1562.opentlc.com/prov/v1/einvoice/create/")
      const res = await axios.post("http://backend-api-route.apps.cluster-bp2lg.bp2lg.sandbox1562.opentlc.com/prov/v1/einvoice/create/" + role, JSON.stringify({ accountname: account, dbname: database, schemaname: schema }), {
        headers: {
          "Content-Type": "application/json"
        }
      });
      console.log("response prov user: ", JSON.stringify(res.data));
      setSuccess(true)
      setLoading(false)


    } catch (error) {
      console.log("error prov user : " + error)
      setSuccess(false)
      setLoading(false)
    }

  }


  if (loading) {
    return (
      <div className="min-h-screen flex items-center justify-center">
        <div
          className="inline-block h-8 w-8 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] text-primary motion-reduce:animate-[spin_1.5s_linear_infinite]"
          role="status">
          <span
            className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]"
          >Loading...</span>
        </div>
      </div>
    );
  }


  return (
    <>
      {success ? (
        navigate("/Home")
      ) : (
        <section className="absolute w-full h-full bg-izeno-gradien-1">
          <Navbar />
          <div className='flex-1 p-8 flex justify-center items-center mx-auto'>
            <div class="p-0">
              <div class="text-izeno-black p-4">
                <div class="container mx-44">
                  <h1 class="text-5xl font-bold">Welcome to e-Invoice</h1>
                  <p class="mt-2">This portal will help you to automatically submit your invoice to LHDN.</p>
                </div>
              </div>
              <div class="container mx-52 max-w-2xl bg-izeno-white relative shadow-lg rounded-lg border-0">
                <h2 class="text-2xl font-bold mb-2 pt-2 text-izeno-black">Hi, User</h2>
                <p class="text-gray-700 mb-2">Before you can use this portal you need to fill this form first to configure your personal settings.</p>

                <div>
                  <form class="mx-auto" onSubmit={handleSubmit}>
                    <div class="mb-2" onMouseLeave={handleMouseLeave}>
                      <label htmlFor="Account" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Account</label>
                      <input type="text" id="Account"
                        onChange={(e) => setAccount(e.target.value)}
                        value={account}
                        onMouseEnter={() => handleMouseEnter('account')}
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-izeno-main focus:border-izeno-main block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-izeno-main dark:focus:border-izeno-main dark:shadow-sm-light" placeholder="Accountname" required />
                      {hoveredField === 'account' && (
                        <div className="absolute left-1 ml-2 p-2 w-64 bg-gray-300 border border-gray-300 rounded-lg shadow-lg">
                          <p className="text-sm text-gray-700">
                            Please put your snowflake account here, for example : <strong>br00321.ap-southeast-1</strong>
                          </p>
                        </div>
                      )}
                    </div>

                    <div class="mb-2" onMouseLeave={handleMouseLeave}>
                      <label htmlFor="Database" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Database</label>
                      <input type="text" id="Database"
                        onChange={(e) => setDatabase(e.target.value)}
                        value={database}
                        onMouseEnter={() => handleMouseEnter('database')}
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-izeno-main focus:border-izeno-main block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-izeno-main dark:focus:border-izeno-main dark:shadow-sm-light" placeholder="Databasename" required />
                      {hoveredField === 'database' && (
                        <div className="absolute left-1 ml-2 p-2 w-64 bg-gray-300 border border-gray-300 rounded-lg shadow-lg">
                          <p className="text-sm text-gray-700">
                            Please use this format [CompanyName]_DB, for example : <strong>IZENO_DB</strong>
                          </p>
                        </div>
                      )}
                    </div>

                    <div class="mb-2" onMouseLeave={handleMouseLeave}>
                      <label htmlFor="Schema" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Schema</label>
                      <input type="text" id="Schema"
                        onChange={(e) => setSchema(e.target.value)}
                        value={schema}
                        onMouseEnter={() => handleMouseEnter('schema')}
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-izeno-main focus:border-izeno-main block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-izeno-main dark:focus:border-izeno-main dark:shadow-sm-light" placeholder="Schemaname" required />
                      {hoveredField === 'schema' && (
                        <div className="absolute left-1 ml-2 p-2 w-64 bg-gray-300 border border-gray-300 rounded-lg shadow-lg">
                          <p className="text-sm text-gray-700">
                            Please use this format [CompanyName]_SH, for example : <strong>IZENO_SH</strong>
                          </p>
                        </div>
                      )}
                    </div>
                    <button type="submit" class="mb-4 mt-2 text-white bg-izeno-main hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>

                  </form>
                </div>
              </div>
            </div>
          </div>
          <FooterSmall absolute />
        </section>
      )}
    </>
  );
}

export default HomePage;
