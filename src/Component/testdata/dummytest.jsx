// test data

const BATCHDATATEST = [{
    "batch_id": "20240607-001",
    "file_link": "INV-iZeno-001",
    "start_date time": "2024-06-07 13:00:00",
    "end_date time": "2024-06-07 14:00:00",
    "status": "Completed",
    "csv_filename": "INV-iZeno-001.csv",
    "submission_uuid": "LHDN-010102-aeftg45",
    "submittedby": "Kennard",
    "tenantid": "iZenoPartner-01"
},
{
    "batch_id": "20240607-002",
    "file_link": "INV-iZeno-002",
    "start_date time": "2024-06-08 13:00:00",
    "end_date time": "2024-06-08 14:00:00",
    "status": "Completed",
    "csv_filename": "INV-iZeno-002.csv",
    "submission_uuid": "LHDN-010102-aef23g45",
    "submittedby": "Kennard",
    "tenantid": "iZenoPartner-01"
},
{
    "batch_id": "20240607-003",
    "file_link": "INV-iZeno-003",
    "start_date time": "2024-06-09 16:00:00",
    "end_date time": "2024-06-00 17:00:00",
    "status": "Completed",
    "csv_filename": "INV-iZeno-003.csv",
    "submission_uuid": "LHDN-010102-ae1213g45",
    "submittedby": "Kennard",
    "tenantid": "iZenoPartner-01"
}];