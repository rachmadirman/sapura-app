# Use an official Node.js image as the base
FROM node:20-alpine

# USER node


# Set the working directory in the container
WORKDIR /app
#RUN chown -R 1001:0 /app
#USER 1001

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Build the React app
RUN npm run build

# Expose port 3000 to the outside world
EXPOSE 3000

# Command to run the React app
CMD ["npm", "start"]
